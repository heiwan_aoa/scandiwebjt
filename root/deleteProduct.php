<html>
  <head>
    <title>Delete Product</title>
    <link rel="stylesheet" type="text/css" href="css/main.css" media="screen" />
    <link href="https://fonts.googleapis.com/css?family=Quicksand:700" rel="stylesheet">
  </head>

  <body>
    <header>
      <nav>
        <ul>
          <li class="navItem"><a href="index.php"><span>list</span></a></li>
          <li class="navItem"><a href="addproduct.php"><span>add</span></a></li>
        </ul>
      </nav>
    </header>
    <div class="mainWrap">
      <?php
      //if delete pin was pressed, then get the passed ID and SKU from the previous pages form.
      if(isset($_POST['delete'])) {
        $id = $_POST['id'];
        $sku = $_POST['sku'];
        //connect to db, start a paragraph with the information about the product, prepare an query.
        require_once('../mysql_connect.php');
        echo "<p>Product: ".$sku." with id: ".$id ;
        $query = "DELETE FROM products WHERE id = $id";
        $execute = mysqli_query($connect,$query);
        //attempt to execute the query and finish the paragraph based on the outcome. Close connection after.
        if (! $execute){
          die(" was not deleted.</p>" .mysql_error());
        }
        echo " deleted!</p>";
        mysqli_close($connect);
      }
      ?>
    </div>
  </body>
</html>
