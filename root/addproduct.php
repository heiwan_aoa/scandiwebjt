<html>
  <head>
    <title>Add Product</title>
    <link rel="stylesheet" type="text/css" href="css/main.css" media="screen" />
    <link href="https://fonts.googleapis.com/css?family=Quicksand:700" rel="stylesheet">
    <script type="text/javascript" src="js/dynamicForm.js"></script>
  </head>

  <body>
    <header>
      <nav>
        <ul>
          <li class="navItem"><a href="index.php"><span>list</span></a></li>
          <li class="navItem"><a href="#"><span>add</span></a></li>
        </ul>
      </nav>
    </header>
    <div class="mainWrap">
      <form action="http://localhost/ScandiWeb_JT/scandiwebjt/root/productadded.php" method="post">
        <h1>ADD A NEW PRODUCT</h1>
        <div class="centerFields">
          <p>sku: </p><input type="text" name="sku" size="30" value=""/>
          <p>name: </p><input type="text" name="name" size="30" value=""/>
          <p>price: </p><input type="text" name="price" size="11" value=""/>
        </div>
        <div class="buttons"><p>type: </p>
          <button id="DVD-Disc">DVD_Disc</button>
          <button id="Book">Book</button>
          <button id="Furniture">Furniture</button>
        </div>
        <select hidden id="typeSel" name="type">
            <option value="DVD-Disc">DVD-Disc</option>
            <option value="Book">Book</option>
            <option value="Furniture">Furniture</option>
          </select>
        <div id="specAtDVD" class="centerFields" style="display:none;">
          <p>size: </p><input class="spec" type="text" name="size" size="11" value=""/>
          <p>Please enter writable DVD Disc Capacity in Megabytes.</p>
        </div>
        <div id="specAtBook" class="centerFields" style="display:none;">
          <p>weight: </p><input class="spec" type="text" name="weight" size="11" value=""/>
          <p>Please enter the weight of the book.</p>
        </div>
        <div id="specAtFurniture" class="centerFields" style="display:none;">
          <p>height: </p><input class="spec" type="text" name="height" size="11" value=""/>
          <p>width: </p><input class="spec" type="text" name="width" size="11" value=""/>
          <p>length: </p><input class="spec" type="text" name="length" size="11" value=""/>
          <p>Enter the dimensions for the furniture you want to add.</p>
        </div>
        <input class="button" type="submit" name="submit" value="add" />
      </form>
    </div>
  </body>
</html>
