//Once the window has loaded, get all the needed elements from DOM.
window.onload = function(){
  function resetValues() {
    i = specs.length;
    while(i--) {
      specs[i].value= '';
    }
  }
  var select = document.getElementById("typeSel");
  var btnDvd = document.getElementById('DVD-Disc');
  var btnBook = document.getElementById('Book');
  var btnFurniture = document.getElementById('Furniture');
  var divSpecAtDVD = document.getElementById('specAtDVD');
  var divSpecAtBook = document.getElementById('specAtBook');
  var divSpecAtFurniture = document.getElementById('specAtFurniture');
  var specs = document.getElementsByClassName('spec'), i = specs.length;
  //reset product type.
  select.value = "";
  //for each type button prevent default behaviour, so that the form isn't submitted on click.
  btnDvd.addEventListener("click", function(event){
    event.preventDefault()
  });
  btnBook.addEventListener("click", function(event){
    event.preventDefault()
  });
  btnFurniture.addEventListener("click", function(event){
    event.preventDefault()
  });
  /*for each type button, change the value of the hidden select in the form, display
  the corresponding special attribute field(s) and hide the rest.*/
  btnDvd.onclick = function() {
    // reset the values of all special fields when the user toggles through all the types.
    resetValues();
    divSpecAtDVD.style.display = "block";
    divSpecAtBook.style.display = "none";
    divSpecAtFurniture.style.display = "none";
    select.value = "DVD-Disc";
  }
  btnBook.onclick = function() {
    resetValues();
    divSpecAtDVD.style.display = "none";
    divSpecAtBook.style.display = "block";
    divSpecAtFurniture.style.display = "none";
    select.value = "Book";
  }
  btnFurniture.onclick = function() {
    resetValues();
    divSpecAtDVD.style.display = "none";
    divSpecAtBook.style.display = "none";
    divSpecAtFurniture.style.display = "block";
    select.value = "Furniture";
  }
}
