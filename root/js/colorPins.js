//Once the window has loaded, get all of the 'pins' that delete a single product and define 2 rgba color values.
window.onload = function(){
  var deletePins = document.getElementsByClassName('pin');
  var red = "rgba(160,10,10,0.6)";
  var green = "rgba(10,130,10,0.6)";
  //for each pin pick one of the three defined colors on random.
  for (i= 0; i < deletePins.length; i++) {
    var rand = Math.floor(Math.random() * Math.floor(2));
    switch (rand) {
      case 0:
        deletePins[i].style.backgroundColor = red;
        break;
      case 1:
        deletePins[i].style.backgroundColor = green;
        break;
      case 2:
        deletePins[i].style.backgroundColor = blue;
        break;
    }
  }
}
