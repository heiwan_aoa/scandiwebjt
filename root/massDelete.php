<html>
  <head>
    <title>Delete Product</title>
    <link rel="stylesheet" type="text/css" href="css/main.css" media="screen" />
    <link href="https://fonts.googleapis.com/css?family=Quicksand:700" rel="stylesheet">
  </head>

  <body>
    <header>
      <nav>
        <ul>
          <li class="navItem"><a href="index.php"><span>list</span></a></li>
          <li class="navItem"><a href="addproduct.php"><span>add</span></a></li>
        </ul>
      </nav>
    </header>
    <div class="mainWrap">
      <?php
      //if mass delete button was pressed, then connect to the db and check if any checkboxes were checked
      if(isset($_POST['deleteMass'])) {
        require_once('../mysql_connect.php');
        if (!empty($_POST['chkDelete'])) {
          //Check! Now count how many products were selected and display the count.
          $checked_count = count($_POST['chkDelete']);
          echo "You have selected following ".$checked_count." option(s): <br/>";
          //itterate through all of the selected products.
          for ($i = 0; $i < $checked_count; $i++) {
            //get each product sku and id from previous page form and start a paragraph displaying them.
            $sku = $_POST['sku'][$i];
            $id = $_POST['chkDelete'][$i];
            echo "<p>Product: ".$sku." with id: ".$id;
            //prepare an query which deletes the product based on it's id, attemt to execute it.
            $query = "DELETE FROM products WHERE id = $id";
            $execute = mysqli_query($connect,$query);
            //end the paragraph based on the query outcome and close the connection to the db.
            if (! $execute){
              die(" was not deleted.</p>" .mysql_error());
            }
            echo " deleted!</p>";
          }
        }
        mysqli_close($connect);
      }
      ?>
    </div>
  </body>
</html>
