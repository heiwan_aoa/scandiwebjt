<html>
  <head>
    <title>Product List</title>
    <link rel="stylesheet" type="text/css" href="css/main.css" media="screen" />
    <link href="https://fonts.googleapis.com/css?family=Quicksand:700" rel="stylesheet">
  </head>
  <body>
    <header>
      <nav>
        <ul>
          <li class="navItem"><a href="#"><span>list</span></a></li>
          <li class="navItem"><a href="addproduct.php"><span>add</span></a></li>
        </ul>
      </nav>
    </header>
    <div class="mainWrap">
      <button class="delSwitchToSingle"><a href="index.php">Mass Delete</a></button>
      <!-- whole product list is a form, so multiple product ids can be sent to MASS DELETION query -->
      <form id="selectedForm" method="POST" action="massDelete.php">
        <input id="delSubm" type="submit" name="deleteMass" value="Delete Selected" />
        <?php
        //connect to db, create query selecting all the collomns from products and attempt to execute it.
        require_once('../mysql_connect.php');
        $query = "SELECT * FROM products";
        $execute = @mysqli_query($connect,$query);
        if($execute) {
          //if the query was successful, itterate through each row.
          while($row = mysqli_fetch_array($execute)){
            //First check which special attribute is set and create a string accordingly.
            if (isset($row['size'])) {
              $special = 'Size: ' . $row['size'] .' MB';
            }elseif (isset($row['weight'])) {
              $special = 'Weight: ' . $row['weight'] .' KG';
            }elseif (isset($row['height'], $row['width'], $row['length'])) {
              $special = 'Dimensions(HxWxL): ' . $row['height'].'x'.$row['width'].'x'.$row['length'];
            }
            //Display all the product values on page inside a div and add a checkbox for mass delete option.
            echo '<div class="product"><input type="checkbox" name="chkDelete[]" value="'.$row['id'].'" />
            <input type="hidden" name="sku[]" value="'.$row['sku'].'" />
            <h2 class="sku">' .
            $row['sku'] . '</h2><h2 class="name">' .
            $row['name'] . '</h2><h2 class="price">' .
            $row['price'] . '<span style="color:rgba(10,70,10,0.8);weight:900;">§</span></h2><h2 class="special">' .
            $special. '</h2></div>';
          }
        }//if execution of the query was unsuccessful, then display error.
        else {
          echo mysqli_error($connect);
        }//close the connection to the db.
        mysqli_close($connect);
        ?>
      </form>
    </div>
  </body>
</html>
