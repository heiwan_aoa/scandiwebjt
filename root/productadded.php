<html>
  <head>
    <title>Add Product</title>
    <link rel="stylesheet" type="text/css" href="css/main.css" media="screen" />
    <link href="https://fonts.googleapis.com/css?family=Quicksand:700" rel="stylesheet">
    <script type="text/javascript" src="js/dynamicForm.js"></script>
  </head>

  <body>
    <header>
      <nav>
        <ul>
          <li class="navItem"><a href="index.php"><span>list</span></a></li>
          <li class="navItem"><a href="#"><span>add</span></a></li>
        </ul>
      </nav>
    </header>
    <div class="mainWrap">
        <?php
        /*If the submit button was pressed, then itterate through all the data entered
        to check what data is missing.*/
        if(isset($_POST['submit'])){
          $data_missing = array();
          if(empty($_POST['sku'])){
            $data_missing[] = 'SKU';
          }else {
            $sku = trim($_POST['sku']);
          }if(empty($_POST['name'])){
            $data_missing[] = 'name';
          }else {
            $name = trim($_POST['name']);
          }if(empty($_POST['price'])){
            $data_missing[] = 'price';
          }else {
            $price = trim($_POST['price']);
          }if(empty($_POST['type'])){
            $data_missing[] = 'type';
          }else {
          /*Check which type was chosen for the product
          and then check if the according special attribute was entered.*/
            $type = trim($_POST['type']);
            //Set initial special attributes to null
            $size = $weight = $height = $width = $length = NULL;
            if($type == "DVD-Disc") {
              if(empty($_POST['size'])){
                $data_missing[] = 'size';
              }else {
                $size = trim($_POST['size']);
              }
            }if($type == "Book") {
              if(empty($_POST['weight'])){
                $data_missing[] = 'weight';
              }else {
                $weight = trim($_POST['weight']);
              }
            }if($type == "Furniture") {
              if(empty($_POST['height'])){
                $data_missing[] = 'height';
              }else {
                $height = trim($_POST['height']);
              }if(empty($_POST['width'])){
                $data_missing[] = 'width';
              }else {
                $width = trim($_POST['width']);
              }if(empty($_POST['length'])){
                $data_missing[] = 'length';
              }else {
                $length = trim($_POST['length']);
              }
            }
          }
          /*  If all the required data was entered, then connect
          to db, create the query, prepare it, insert the parameters to add a product and execute it.*/
          if(empty($data_missing)){
            require_once('../mysql_connect.php');
            $query = "INSERT INTO products (id, sku ,name, price, type, size, weight, height, width, length)
            VALUES (NULL,?,?,?,?,?,?,?,?,?)";
            $statement = mysqli_prepare($connect, $query);
            mysqli_stmt_bind_param($statement, "ssisiiiii", $sku, $name, $price, $type, $size,
            $weight, $height, $width, $length);
            mysqli_stmt_execute($statement);
            $affected_rows = mysqli_stmt_affected_rows($statement);
            //Display information if the query was successful and then close the statement and connection to db.
            if($affected_rows == 1) {
              echo '<p style="width:55%; margin:0 auto; display:block;">Product Added</p>';
            }else {
              echo '<p style="width:55%; margin:0 auto; display:block;">Fill out form before submitting</p>';
            }
            mysqli_stmt_close($statement);
            mysqli_close($connect);
          }else {
            echo '<p style="width:55%; margin:0 auto; display:block;">You need to enter this data:</p>';
            foreach ($data_missing as $missing) {
              echo '<p style="width:55%; margin:0 auto; display:block;">'.$missing.'</p>';
            }
          }
        }
        ?>
        <form action="http://localhost/ScandiWeb_JT/scandiwebjt/root/productadded.php" method="post">
          <h1>ADD A NEW PRODUCT</h1>
          <div class="centerFields">
            <p>sku: </p><input type="text" name="sku" size="30" value=""/>
            <p>name: </p><input type="text" name="name" size="30" value=""/>
            <p>price: </p><input type="text" name="price" size="11" value=""/>
          </div>
          <div class="buttons"><p>type: </p>
            <button id="DVD-Disc">DVD_Disc</button>
            <button id="Book">Book</button>
            <button id="Furniture">Furniture</button>
          </div>
          <select hidden id="typeSel" name="type">
              <option value="DVD-Disc">DVD-Disc</option>
              <option value="Book">Book</option>
              <option value="Furniture">Furniture</option>
            </select>
          <div id="specAtDVD" class="centerFields" style="display:none;">
            <p>size: </p><input class="spec" type="text" name="size" size="11" value=""/>
          </div>
          <div id="specAtBook" class="centerFields" style="display:none;">
            <p>weight: </p><input class="spec" type="text" name="weight" size="11" value=""/>
          </div>
          <div id="specAtFurniture" class="centerFields" style="display:none;">
            <p>height: </p><input class="spec" type="text" name="height" size="11" value=""/>
            <p>width: </p><input class="spec" type="text" name="width" size="11" value=""/>
            <p>length: </p><input class="spec" type="text" name="length" size="11" value=""/>
          </div>
          <p><input class="button" type="submit" name="submit" value="add" /></p>
        </form>
    </div>
  </body>
</html>
